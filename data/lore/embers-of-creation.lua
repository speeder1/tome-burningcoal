newLore
{
	id = "embers-of-creation-mad-scientist",
	category = "burning coal",
	name = "mad scientist journal",
	bloodstains = 2,
	lore = [[I failed, again...

	Here I am in this place, bizarre place, trying to do the impossible, why I accepted this idea? Or, did I?

	I am not even sure anymore if I am actually real, nothing here makes sense, there are things that happen here that defy logic, maybe I don't really exist.

	Yet, yet, if I do, and if I succeed, I would have inimaginable power, there is someone that found a way to steal the power of a god!

	I can do the same, I am sure! I just need to make more potions, the ones I made so far usually just poison me, but I am sure this last one...

	this last one... ooooh, it is so perfect, it is a thing of beauty.

	My masterwork, the extract of some god power! I don't even know what one, but I do know, this is a God's Extract, it could unlock awesome powers!

	I just need to drink it! Wait, I am hearing someone coming, better summon the golems.]],

	on_learn = function(who)
		if not game:isCampaign("Maj'Eyal") then return end
		game.player:setQuestStatus("start-burningcoal", engine.Quest.COMPLETED, "killed-alchemist")
	end,
}

newLore
{
	id = "embers-of-creation-burning-heart",
	category = "burning coal",
	name = "Burning Heart", always_pop = true,
	image = "embers-burning-heart.png",
	lore = function() return [[Your chest burns with searing heat.

	It is hard understand what is happening, but the constant pain make it clear, your heart, inside your chest, changed its material, from flesh into a burning coal.]] .. (game.party:knownLore("creation-human") and [[ It is exactly like the legends say, in the cave your heart became again the human heart as Gerlyk had made it.]] or [[ You feel like you can learn more about this.]]) end,
}

newLore
{
	id = "embers-of-creation-godly-skin",
	category = "burning coal",
	name = "Godly Skin", always_pop = true,
	image = "embers-burning-skin.png",
	lore = function() return [[The constant powerful aura, remains of ]] .. (game.party:knownLore("creation-human") and [[ Gerlyk,]] or [[ an unknown god,]]) .. [[ were absorbed into your skin, fire flows through it making it glow like burning embers.

	Godly Skin can absorb fire and burn any who touches it, but it is weaker to cold.]] end,
}

newLore
{
	id = "embers-of-creation-godly-body",
	category = "burning coal",
	name = "Godly Body", always_pop = true,
	image = "embers-burning-body.png",
	lore = function()
		local godName = (game.party:knownLore("creation-human") and [[Gerlyk]] or [[God]])

		return [[ ]] .. godName .. [['s crystalized essence merges with your body, it turns you into a creature that is partially a deity, giving you the potential for having great power.

		You feel as if you can now accumulate much more power than a normal human ever would, but you also feel that your powers are linked to fire, light and nature, making you weak if attacked by their opposites.

		Others certainly would love to put their hands on this]] .. (game.party:knownLore("embers-of-creation-mad-scientist") and [[, it is not surprising you found an alchemist that tried to make potions out of this.]] or [[.]]) end,
}

newLore
{
	id = "embers-of-creation-godly-bones",
	category = "burning coal",
	name = "Godly Bones", always_pop = true,
	image = "embers-self-stab.png",
	lore = function()
	local godName = (game.party:knownLore("creation-human") and [[Gerlyk]] or [[god]])

	return [[You stab your heart with the bloodied knife, and then the blood of ]] .. godName .. [[ turns liquid and alive again, and get absorbed into your bloodstream.

	Your entire body changes a little, but you can really feel it, your bones, inside your living body, merge with the dead holy matter, and become holy and blessed, empowering you with holy light.]] end,
}

newLore
{
	id = "embers-vault-sign",
	category = "burning coal",
	name = "warning sign-post (vault)", always_pop = true,
	lore = [[There is an inscription here:
#{italic}#You are in a secure storage facility. The owners won't take responsability for deaths due to foolish attempts in stealing the contents of the next rooms.#{normal}#]],
}

newLore
{
	id = "embers-lava-sign",
	category = "burning coal",
	name = "warning sign-post (fire)", always_pop = true,
	lore = [[There is an inscription here:
#{italic}#This teleporter leads to an extremely dangerous place, expect extreme temperatures, do not approach without protection against fire and heat.#{normal}#]],
}
