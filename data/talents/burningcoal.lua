newTalentType{ type="human/burningcoal", name = "burning coal", generic = true, description = "Godly human's innate habilities." }

newTalent
{
	name = "Burning Coal",
	type = {"base/class", 1},
	info = "Some basic race stuff of burning coal, you are not even supposed to read this!",
	mode = "passive",
	hide = "always",
	no_unlearn_last = true,

	callbackOnKill = function(self, t)
		self.line1 = (self.line1 or 0) + 1
		if not self.all_kills then return end
		self.line2 = (self.line2 or 0) + 1

		self.tier1BurningCoalQuest = self.tier1BurningCoalQuest or game.player:hasQuest("tier1-burningcoal")
		local tier1BurningCoalQuest = self.tier1BurningCoalQuest
		self.line3 = (self.line3 or 0) + 1
		if tier1BurningCoalQuest and not tier1BurningCoalQuest:isEnded() then
			self.line4 = (self.line4 or 0) + 1
			if not tier1BurningCoalQuest:isCompleted("trollmire") then
				self.line5 = (self.line5 or 0) + 1
				if self.all_kills["Prox the Mighty"] then
					self.line6 = (self.line6 or 0) + 1
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "trollmire")
				elseif self.all_kills["Shax the Slimy"] and not tier1BurningCoalQuest:isCompleted("trollmire-flooded") then
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "trollmire")
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "trollmire-flooded")
				end
			end

			if not tier1BurningCoalQuest:isCompleted("kor-pul") then
				if self.all_kills["The Shade"] then
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "kor-pul")
				elseif self.all_kills["The Possessed"] and not tier1BurningCoalQuest:isCompleted("kor-pul-invaded") then
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "kor-pul")
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "kor-pul-invaded")
				end
			end

			if not tier1BurningCoalQuest:isCompleted("spellblaze") then
				if self.all_kills["Spellblaze Crystal"] then
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "spellblaze")
				end
			end

			if not tier1BurningCoalQuest:isCompleted("rhaloren") then
				if self.all_kills["Rhaloren Inquisitor"] then
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "rhaloren")
				end
			end

			if not tier1BurningCoalQuest:isCompleted("norgos") then
				if self.all_kills["Norgos, the Guardian"] then
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "norgos")
				elseif self.all_kills["Norgos, the Frozen"] and not tier1BurningCoalQuest:isCompleted("norgos-invaded") then
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "norgos")
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "norgos-invaded")
				end
			end

			if not tier1BurningCoalQuest:isCompleted("heart-gloom") then
				if self.all_kills["The Withering Thing"] then
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "heart-gloom")
				elseif self.all_kills["The Dreaming One"] and not tier1BurningCoalQuest:isCompleted("heart-gloom-purified") then
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "heart-gloom")
					tier1BurningCoalQuest:setStatus(engine.Quest.COMPLETED, "heart-gloom-purified")
				end
			end

			if tier1BurningCoalQuest:allDone() then
				game.player:setQuestStatus("tier1-burningcoal", engine.Quest.DONE)

				if not game.player:hasQuest("starter-zones") then
					game.player:grantQuest("starter-zones")
				end
			end
		end
	end,

	callbackOnLevelup = function(self, t, new_level)
		if new_level > 12 and not game.player:hasQuest("starter-zones") then
			game.player:grantQuest("starter-zones")
		end
	end
}

newTalent
{
	name = "Heart of Ember",
	type = {"human/burningcoal", 1},
	mode = "passive",
	hide = true,
	require = {level = function(level) return math.floor(level*level+0.5) end, },
	points = 5,

	extraDamage = function(self, t) return self:getTalentLevel(t)*self:getTalentLevel(t)*self:getCon()/7+5 end,
	selfDamage = function(self, t) return t.extraDamage(self, t)/10+1 end,

	selfHealAfterFireEffects = function(self, t)
		--formula is: input fire damage * affinity = heal, input fire damage - resisted damage = damage, result is heal - damage
		local fireDamage = t.selfDamage(self, t)
		local fireDamage = fireDamage * ((self.inc_damage[DamageType.FIRE] or 0)+100)/100
		local affinity_heal = math.max(0, fireDamage * ((self.damage_affinity.all or 0) + (self.damage_affinity[DamageType.FIRE] or 0)) / 100)

		if self.resists then
			local pen = 0
			if self.combatGetResistPen then pen = self:combatGetResistPen(DamageType.FIRE)
			elseif self.resists_pen then pen = (self.resists_pen.all or 0) + (self.resists_pen[DamageType.FIRE] or 0)
			end

			local res = self:combatGetResist(type)
			pen = util.bound(pen, 0, 100)
			if res > 0 then	res = res * (100 - pen) / 100 end
			if res >= 100 then fireDamage = 0
			else fireDamage = fireDamage * ((100 - res) / 100)
			end
		end

		if self.resists_self then
			local res = (self.resists_self[DamageType.FIRE] or 0) + (self.resists_self.all or 0)
			if res >= 100 then fireDamage = 0
			elseif res <= -100 then fireDamage = fireDamage * 2
			else fireDamage = fireDamage * ((100 - res) / 100)
			end
		end

		return affinity_heal - fireDamage
	end,

	callbackOnActBase = function(self, t)
		--DamageType:get(DamageType.FIRE).projector(self, self.x, self.y, DamageType.FIRE, t.selfDamage(self, t))
		self:updateTalentPassives(t)
	end,

	passives = function(self, t, p)
		--oldmodifiedonTakeHit = oldmodifiedonTakeHit or self.onTakeHit
		--self.onTakeHit = modifiedonTakeHit
		self:talentTemporaryValue(p, "melee_project", {[DamageType.FIRE]=t.extraDamage(self, t)})
		self:talentTemporaryValue(p, "life_regen", t.selfHealAfterFireEffects(self, t))

		--self:talentTemporaryValue(p, "inc_stats", {[self.STAT_CON]=t.statBonus(self, t)})
		--self:talentTemporaryValue(p, "flat_damage_cap", {all=t.getMaxDamage(self, t)})
	end,
	info = function(self, t)
		if t.selfHealAfterFireEffects(self, t) < 0 then
			return ([[Your body has a heart of burning coal, it causes you %d of fire damage, but also allows you to cause %d of fire damage when hitting enemies.

				The constitution of your heart makes it burn brighter, increasing both damages.]])
			:format(t.selfDamage(self, t), t.extraDamage(self, t))
		else
			return ([[Your body has a heart of burning coal, it causes you %d of fire damage, but also allows you to cause %d of fire damage when hitting enemies.

				The constitution of your heart makes it burn brighter, increasing both damages.

				Because your have an affinity to fire, instead of actually being hurt, your heart instead regenerates %d of life.]])
			:format(t.selfDamage(self, t), t.extraDamage(self, t), t.selfHealAfterFireEffects(self, t))
		end
	end,
}

newTalent
{
	name = "Godly Skin",
	type = {"human/burningcoal", 1},
	mode = "passive",
	hide = true,
	require = {level = function(level) return math.floor(level*level+0.5) end, },
	points = 5,
	fireAffinity = function(self, t) return self:getTalentLevel(t)*20+20 end,
	coldResistPenalty = function(self, t) return self:getTalentLevel(t)*5 end,
	returnFireDamage = function(self, t) return self:getTalentLevel(t)*8 end,
	passives = function(self, t, p)
		self:talentTemporaryValue(p, "damage_affinity", {[DamageType.FIRE]=t.fireAffinity(self, t)})
		self:effectTemporaryValue(p, "resists", {[DamageType.COLD]=-t.coldResistPenalty(self, t)})
		self:effectTemporaryValue(p, "on_melee_hit", {[DamageType.FIRE]=t.returnFireDamage(self, t)})
	end,
	info = function(self, t)
		return ([[Your skin taps into godly power, attuned with fire it can absorb %d%% of fire damage, but it is also %d%% less resistant to cold attacks.

			The fiery skin also damages enemies that dare to touch it by %d of fire damage.]])
		:format(t.fireAffinity(self, t), t.coldResistPenalty(self, t), t.returnFireDamage(self, t))
	end,
}

newTalent
{
	name = "Godly Body",
	type = {"human/burningcoal", 1},
	mode = "passive",
	hide = true,
	require = {level = function(level) return math.floor(level*level+0.5) end, },
	points = 5,
	callbackOnLevelup = function(self, t, new_level)
		if new_level == 30 then
			self.unused_prodigies = self.unused_prodigies + 1
			Dialog:simpleLongPopup("Level 30!", "You have achieved #LIGHT_GREEN#level 30#WHITE#, congratulations!\n\nThis level is special, it granted you a prodigy, but your Godly Body talent gave you an extra prodigy!", 400)
		end
		if new_level % 3 == 0 then
			self.unused_talents = self.unused_talents + 1
			self.unused_generics = self.unused_generics + 1
		end
		return
	end,
	resistsBonus = function(self, t) return self:getTalentLevel(t)*5 end,
	damageIncrease = function(self, t) return self:getTalentLevel(t)*3 end,
	passives = function(self, t, p)
		self:effectTemporaryValue(p, "resists", {[DamageType.COLD]=t.resistsBonus(self, t), [DamageType.FIRE]=t.resistsBonus(self, t), [DamageType.LIGHT]=t.resistsBonus(self, t), [DamageType.DARKNESS]=t.resistsBonus(self, t), [DamageType.NATURE]=t.resistsBonus(self, t), [DamageType.BLIGHT]=t.resistsBonus(self, t)})
		self:effectTemporaryValue(p, "inc_damage_actor_type", {["unnatural"]=t.damageIncrease(self, t)})
		self:effectTemporaryValue(p, "inc_damage", {[DamageType.FIRE]=t.damageIncrease(self, t), [DamageType.LIGHT]=t.damageIncrease(self, t)})
	end,
	info = function(self, t)
		return ([[You have a godly body that can resist %d%% more of cold, fire, light, dark, nature and blight damage.

			The energies also empower your fire and light damage by %d%% and your natural emnity for unnatural enemies also increase damage against them by the same amount.]])
		:format(t.resistsBonus(self, t), t.damageIncrease(self, t))
	end,
}

newTalent
{
	name = "Godly Bones",
	type = {"human/burningcoal", 1},
	mode = "passive",
	hide = true,
	require = {level = function(level) return math.floor(level*level+0.5) end, },
	points = 5,

	callbackOnActBase = function(self, t)
		self:updateTalentPassives(t)
	end,

	resistsBonus = function(self, t) return self:getTalentLevel(t)*5 end,
	damageIncrease = function(self, t) return self:getTalentLevel(t)*3 end,

	encumbrancePercentage = function(self, t) return 1-(self:getEncumbrance()/self:getMaxEncumbrance()) end,
	holyDamagePercentage = function(self, t) return self:getTalentLevel(t)*20+10 end,
	encumbrance = function(self, t) return self:getTalentLevel(t)*self:getStr()/3 end,
	extraGlobalSpeed = function(self, t) return t.encumbrancePercentage(self, t)*self:getTalentLevel(t)*self:getDex()/5+self:getTalentLevel(t)*2 end,
	fatigueImprovement = function(self, t) return t.encumbrancePercentage(self, t)*self:getTalentLevel(t)*self:getCon()/5+self:getTalentLevel(t) end,
	holyOnMeleeHit = function(self, t) return t.holyDamagePercentage(self, t)/100*(self.on_melee_hit[DamageType.FIRE] or 0) end,
	holyProjectHit = function(self, t) return t.holyDamagePercentage(self, t)/100*(self.melee_project[DamageType.FIRE] or 0) end,

	passives = function(self, t, p)
		self:effectTemporaryValue(p, "max_encumber", t.encumbrance(self, t))
		self:checkEncumbrance() --forced update of this, since we will use it later.

		self:effectTemporaryValue(p, "global_speed_base", t.extraGlobalSpeed(self, t)/100)
		self:recomputeGlobalSpeed() --why not?

		self:effectTemporaryValue(p, "fatigue", -t.fatigueImprovement(self, t))
		self:effectTemporaryValue(p, "max_stamina", t.fatigueImprovement(self, t))

		self:effectTemporaryValue(p, "on_melee_hit", {[DamageType.HOLY_LIGHT]=t.holyOnMeleeHit(self, t)})
		self:talentTemporaryValue(p, "melee_project", {[DamageType.HOLY_LIGHT]=t.holyProjectHit(self, t)})
	end,
	info = function(self, t)
		local desc = ([[The godly bones inside the body make %d%% of your projection and return fire damages also do Holy damage.

				They are also lighter and stronger, improving %d of encumbrance (depends on strength), %d%% of added global speed (depends on dexterity and current encumbrance) and an improvement of %d to both fatigue reduction and maximum stamina (depends on constitution and current encumbrance).]])
			:format(t.holyDamagePercentage(self, t), t.encumbrance(self, t), t.extraGlobalSpeed(self, t), t.fatigueImprovement(self, t))

		if self.on_melee_hit[DamageType.FIRE] and self.on_melee_hit[DamageType.FIRE] > 1 then
			desc = desc .. ("\nYou do %d of return Holy damage when something touches you."):format(t.holyOnMeleeHit(self, t))
		end

		if self.melee_project[DamageType.FIRE] and self.melee_project[DamageType.FIRE] > 1 then
			desc = desc .. ("\nYou do %d of Holy damage when you attack something."):format(t.holyProjectHit(self, t))
		end

		return desc
	end,
}
