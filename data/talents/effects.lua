newEffect
{
	name = "ZONE_GOD_LINGERING_POWER",
	desc = "Opressive Godly Power",
	no_stop_enter_worlmap = true,
	long_desc = function(self, eff) return ("Zone-wide effect: Remains of some god power increases fire potency of everything by +15 and make everything easier to burn by +15 too") end,
	decrease = 0, no_remove = true,
	type = "other",
	subtype = { aura=true },
	status = "detrimental",
	zone_wide_effect = true,
	parameters = {},
	activate = function(self, eff)
		self:effectTemporaryValue(eff, "inc_damage", {[DamageType.FIRE]=15})
		self:effectTemporaryValue(eff, "resists", {[DamageType.FIRE]=-15})
		if self == game.player and not self.GerlykCounter then self.GerlykCounter = 0 end
	end,
	deactivate = function(self, eff)
	end,
	on_timeout = function(self, eff)
		if self.GerlykCounter then
			self.GerlykCounter = self.GerlykCounter+1
			if self.GerlykCounter > 1200 and not game.party:knownLore("embers-of-creation-godly-skin") then
				game.party:learnLore("embers-of-creation-godly-skin")
				game.player:setQuestStatus("start-burningcoal", engine.Quest.COMPLETED, "absorbed-gerlyk-aura")
				game.player:learnTalentType("human/burningcoal", true)
				game.player:learnTalent(game.player.T_GODLY_SKIN, true, 1, {no_unlearn=true})
				if not game.player.__show_special_talents then game.player.__show_special_talents = {} end
				game.player.__show_special_talents[game.player.T_GODLY_SKIN] = true
			end
		end
	end
}
