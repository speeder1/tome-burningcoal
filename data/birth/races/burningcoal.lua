local ActorTalents = require "engine.interface.ActorTalents"

newBirthDescriptor
{
	type = "subrace",
	name = "Burning Coal",
	desc =
	{
		"Cornacs are Humans from the northern parts of the Allied Kingdoms.",
		"Humans are an inherently very adaptable race and as such they gain a #GOLD#talent category point#WHITE# at birth (others only gain one at levels 10, 20 and 36).",
		"But legend says, that humans were made from a #ORANGE#Burning Coal#WHITE#, and that the passion on their hearts made them stronger, this subrace doesn't exist yet, but it will, if the #GOLD#quest#WHITE# to find the ancient powers is completed.",
		"#GOLD#Stat modifiers:",
		"#LIGHT_BLUE# * +0 Strength, +0 Dexterity, +0 Constitution",
		"#LIGHT_BLUE# * +0 Magic, +0 Willpower, +0 Cunning",
		"#GOLD#Life per level:#LIGHT_BLUE# 10",
		"#GOLD#Base life regeneration:#LIGHT_BLUE# 2",
		"#GOLD#Experience penalty:#LIGHT_BLUE# 0%",
		"#GOLD#Starts with unique strong shield rune.",
		"#LIGHT_GREEN#Extra quests, dungeons, and strong #RED#fire #LIGHT_GREEN#and #YELLOW#light #LIGHT_GREEN#based class talents.",
	},
	experience = 1,
	inc_stats = { str=0, dex=0, con=0, mag=0, wil=0, dex=0, cun=0, lck=0 },
	talents =
	{
		[ActorTalents.T_BURNING_COAL]=1,
	},
	copy =
	{
		_forbid_start_override = true,
		moddable_tile = "human_#sex#",
		moddable_tile_base = "base_cornac_01.png",
		random_name_def = "cornac_#sex#",
		unused_talents_types = 1,
		life_rating = 10,
		life_regen = 2,
		default_wilderness = {"farportal-end", "last-hope"},
		starting_zone = "embers-of-creation",
		starting_quest = "start-burningcoal",
		starting_intro = "burningcoal",
		resolvers.inscription("RUNE:_SHIELDING", {cooldown=50, dur=25, power=500}),

	},
}
getBirthDescriptor("race", "Human").descriptor_choices.subrace["Burning Coal"] = "allow"
