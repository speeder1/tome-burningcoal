local class = require"engine.class"
local ActorTalents = require "engine.interface.ActorTalents"
local ActorTemporaryEffects = require "engine.interface.ActorTemporaryEffects"
local Birther = require "engine.Birther"
local DamageType = require "engine.DamageType"
local Zone = require "engine.Zone"
local PartyLore = require "mod.class.interface.PartyLore"


class:bindHook("ToME:load", function(self, data)
ActorTalents:loadDefinition("/data-burningcoal/talents/burningcoal.lua")
ActorTemporaryEffects:loadDefinition("/data-burningcoal/talents/effects.lua")
PartyLore:loadDefinition("/data-burningcoal/lore/embers-of-creation.lua")
Birther:loadDefinition("/data-burningcoal/birth/races/burningcoal.lua")
end)
