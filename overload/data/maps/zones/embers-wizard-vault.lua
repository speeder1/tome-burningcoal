local function isUnnaturalEntity(entity)
	return entity.type == "undead" or entity.type == "construct"
end

defineTile('.', "FLOOR")
defineTile('#', "HARDWALL")
defineTile('+', "DOOR")
defineTile('<', "UP")
defineTile('>', "DOWN")
defineTile('D', "DOOR_VAULT")
defineTile('L', "KEY_LOCKED_DOOR")
defineTile('v', "SIGN_POST_VAULT")
defineTile('B', "FLOOR", nil, "MAD_SCIENTIST")
defineTile('@', "FLOOR", nil, {random_filter={max_ood=2, special=isUnnaturalEntity}})
defineTile('?', "FLOOR", {random_filter={add_levels=2, tome_mod="vault"}}, {random_filter={special=isUnnaturalEntity, random_elite={rank=3, name_scheme="#rng# the Vault Guard"}}})
defineTile('b', "FLOOR", {random_filter={add_levels=2, tome_mod="gvault"}}, {random_filter={special=isUnnaturalEntity, random_boss={rank=3.5, name_scheme="#rng# the Vault Guard"}}})
defineTile('&', "FLOOR", {random_filter={add_levels=5,ego_chance=70}})
defineTile('!', "FLOOR", {random_filter={add_levels=5,unique=true,not_properties={"lore"}}})
defineTile('$', "FLOOR", {random_filter={type="money"}})
defineTile('*', "FLOOR", {random_filter={type="gem"}})

--left side vaults
addZone({2, 2, 10, 5}, "zonename", "Vault 1")
addZone({2, 7, 10, 10}, "zonename", "Vault 2")
addZone({2, 12, 10, 15}, "zonename", "Vault 3")
--right side vaults
addZone({14, 2, 22, 5}, "zonename", "Vault 4")
addZone({14, 7, 22, 10}, "zonename", "Vault 5")
addZone({14, 12, 22, 15}, "zonename", "Vault 6")
--boss side
addZone({2, 16, 6, 30}, "zonename", "Vault 7")
addZone({19, 16, 22, 30}, "zonename", "Vault 8")
addZone({8, 16, 16, 30}, "zonename", "The Hall of Trials")

startx = 12
starty = 2
endx = 12
endy = 28

return {
[[#########################]],
[[#########################]],
[[##....#.@..#<#..&.#....##]],
[[##..&.+....D.D....+..*.##]],
[[##.*..#..$.#.#.?..#..@.##]],
[[##....#....#.#..*.#....##]],
[[############.############]],
[[##@.*.#.?..#.#....#..*.##]],
[[##....+....D.D.@..+....##]],
[[##..&.#....#.#....#...?##]],
[[##$...#..*.#.#..&.#..?.##]],
[[############.############]],
[[##..@.#..@v#.#v...#..&.##]],
[[##....+.*..D.D....+....##]],
[[##v.$.#..*.#.#....#.$.v##]],
[[##....#.@..#.#..$.#..*.##]],
[[##L#########D#########L##]],
[[##...?###.......###?...##]],
[[##.&..####.....####....##]],
[[##..*.###.......###....##]],
[[##...b####.....####b...##]],
[[##.######.......######.##]],
[[##.$!$####..B..####&&$.##]],
[[##.$$$###@@@.@@@###$$$.##]],
[[##.$&&######L######$@$.##]],
[[##@$$$######.######$$$.##]],
[[##.#########.#########.##]],
[[##.&*!#####...#####*&*.##]],
[[##.***#####.>.#####***.##]],
[[##.&*b#####...#####b!*.##]],
[[##&**b#############b&&.##]],
[[#########################]],
[[#########################]],
}