-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2016 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

local DamageType = require "engine.DamageType"

load("/data/general/objects/objects-maj-eyal.lua")

newEntity{ base = "BASE_LITE",
	define_as = "MAGICAL_OBSIDIAN",
	power_source = {nature=true, arcane=true},
	unique = true,
	name = "Magical Obsidian", image="object/artifact/magic_obsidian.png",
	unided_name = "weird black rock",
	level_range = {1, 50},
	color=colors.BLACK,
	encumber = 2,
	rarity = 100,
	desc = [[A magical obsidian rock, it looks black as expected from an obsidian, but lights up the path like if it was molten lava, yet it is cold to the touch.]],
	cost = 2000,

	wielder = {
		lite = 3,
		healing_factor = 0.1,				
		blind_immune = 0.3,
		confusion_immune = 0.3,
		see_stealth = 5,
		see_invisible = 5,
		infravision = 5,
		trap_detect_power = 10,
		disarm_bonus = 10,
		resists = {
			[DamageType.FIRE]=20,
			[DamageType.COLD]=15,
			[DamageType.LIGHT]=15,
			[DamageType.DARKNESS]=15,
		},
		resists_cap = {
			[DamageType.FIRE]=10,
			[DamageType.COLD]=5,
			[DamageType.LIGHT]=5,
			[DamageType.DARKNESS]=5,
		},
	},
}
