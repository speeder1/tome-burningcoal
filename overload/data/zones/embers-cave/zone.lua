-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2016 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org


return {
	name = "Embers of Creation Cavern",
	level_range = {2, 4},
	level_scheme = "player",
	max_level = 2,
	decay = {300, 800},
	actor_adjust_level = function(zone, level, e) return zone.base_level + e:getRankLevelAdjust() + level.level-1 + rng.range(-1,2) end,
	width = 50, height = 50,
	all_lited = true,
	no_worldport = true,
	persistent = "zone",
	ambient_music = "Mystery.ogg",
	min_material_level = 1,
	max_material_level = 1,
	generator =  {
		map = {
			class = "engine.generator.map.Cavern",
			zoom = 14,
			min_floor = 700,
			floor = "CRYSTAL_FLOOR",
			wall = {"CRYSTAL_WALL","CRYSTAL_WALL2","CRYSTAL_WALL3","CRYSTAL_WALL4","CRYSTAL_WALL5","CRYSTAL_WALL6","CRYSTAL_WALL7","CRYSTAL_WALL8","CRYSTAL_WALL9","CRYSTAL_WALL10","CRYSTAL_WALL11","CRYSTAL_WALL12","CRYSTAL_WALL13","CRYSTAL_WALL14","CRYSTAL_WALL15","CRYSTAL_WALL16","CRYSTAL_WALL17","CRYSTAL_WALL18","CRYSTAL_WALL19","CRYSTAL_WALL20",},
			up = "CRYSTAL_LADDER_UP",
			down = "CRYSTAL_LADDER_DOWN",
			door = "CRYSTAL_FLOOR",
		},
		actor = {
			class = "mod.class.generator.actor.Random",
			nb_npc = {20, 30},
			filters = { {max_ood=2}, },
			--guardian = "SPELLBLAZE_CRYSTAL",
		},
		object = {
			class = "engine.generator.object.Random",
			nb_object = {6, 9},
		},
		trap = {
			class = "engine.generator.trap.Random",
			nb_trap = {0, 0},
		},
	},
	levels =
	{
		[1] = {
			generator = { map = {
				up = "CRYSTAL_FLOOR",
			}, },
		},
		[2] = {
			generator = { map = {
				down = "EXIT_TO_WIZARDS_MAZE",
				force_last_stair = true,
			}, },
		},
	},

	foreground = function(level, dx, dx, nb_keyframes)
		local tick = core.game.getTime()
		local sr, sg, sb
		sr = 4 + math.sin(tick / 2000) / 2
		sg = 3 + math.sin(tick / 2700)
		sb = 3 + math.sin(tick / 3200)
		local max = math.max(sr, sg, sb)
		sr = sr / max
		sg = sg / max
		sb = sb / max

		level.map:setShown(sr, sg, sb, 1)
		level.map:setObscure(sr * 0.6, sg * 0.6, sb * 0.6, 1)
	end,

	post_process = function(level)
		if level.level == 2 then
			local boss = game.zone:makeEntity(game.level, "actor", {subtype = "crystal", random_boss = {nb_classes=0, rank=3.5, loot_quantity = 2}}, nil, true)
			boss.exp_worth = 2

			local reward = game.zone:makeEntityByName(game.level, "object", "MAGICAL_OBSIDIAN")

			if reward then
				boss:addObject(boss.INVEN_INVEN, reward)
				game.zone:addEntity(game.level, reward, "object")	
			end
			
			game.zone:addEntity(game.level, boss, "actor", level.default_down.x, level.default_down.y)
		end
	end,

	on_enter = function(lev, old_lev, newzone)
		local Dialog = require("engine.ui.Dialog")
		if lev == 1 and not game.level.shown_warning then
			game.party:learnLore("embers-of-creation-burning-heart")
			game.level.shown_warning = true
			game.player:setQuestStatus("start-burningcoal", engine.Quest.COMPLETED, "entered")
			game.player:learnTalentType("human/burningcoal", true)
			game.player:learnTalent(game.player.T_HEART_OF_EMBER, true, 1, {no_unlearn=true})
			if not game.player.__show_special_talents then game.player.__show_special_talents = {} end
			game.player.__show_special_talents[game.player.T_HEART_OF_EMBER] = true
		end
	end
}