return {
	name = "Embers of Creation - Wizard's Home",
	level_range = {5, 7},
	level_scheme = "player",
	max_level = 6,
	decay = {300, 800},
	actor_adjust_level = function(zone, level, e) return zone.base_level + e:getRankLevelAdjust() + level.level-1 + rng.range(-1,2) + (level.extra_diff_level or 0) end,
	width = 50, height = 50,		
	persistent = "zone",
	ambient_music = "Alchemist.ogg",
	max_material_level = 1,
	generator =  {
		map = {
			class = "engine.generator.map.Roomer",
			nb_rooms = 10,
			rooms = {"random_room", {"money_vault",5}, {"lesser_vault",8}},
			lesser_vaults_list = {"circle","amon-sul-crypt","rat-nest","skeleton-mage-cabal"},
			lite_room_chance = 80,
			['.'] = "FLOOR",
			['#'] = "WALL",
			up = "UP",
			down = "DOWN",
			door = "DOOR",
		},
		actor = {
			class = "mod.class.generator.actor.Random",
			nb_npc = {10, 40},
		},
		object = {
			class = "engine.generator.object.Random",
			nb_object = {6, 9},
		},
		trap = {
			class = "engine.generator.trap.Random",
			nb_trap = {0, 0},
		},
	},
	levels =
	{
		[1] = {
			all_lited = true,
			generator = 
			{
				map = 
				{
					class = "engine.generator.map.Static",
					map = "zones/embers-wizard-house",
					up = "EXIT_TO_MAZE",
				}, 
			},
		},
		[2] = {
			ambient_music = "Broken.ogg",
			no_level_connectivity = true,
			generator = {
				map = {
					class = "engine.generator.map.Static",
					map = "zones/embers-wizard-vault",
				},
				actor =
				{
					nb_npc = {0, 0}
				},
				object =
				{
					nb_object = {10, 20}
				}
			},
		},
		[3] = {	
			ambient_music = "To the Depths.ogg",
			effects = {"EFF_ZONE_GOD_LINGERING_POWER"},
			color_shown = {0.75, 0.7, 0.7, 1},
			color_obscure = {0.75*0.6, 0.7*0.6, 0.7*0.6, 0.6},
			generator = {
				trap =
				{
					nb_trap = {5, 10}
				}
			},
		},
		[4] = {
			ambient_music = "To the Depths.ogg",
			effects = {"EFF_ZONE_GOD_LINGERING_POWER"},
			color_shown = {0.75, 0.7, 0.7, 1},
			color_obscure = {0.75*0.6, 0.7*0.6, 0.7*0.6, 0.6},
			generator = {
				trap =
				{
					nb_trap = {7, 12}
				}
			},
		},
		[5] = {
			ambient_music = "To the Depths.ogg",
			extra_diff_level = 2,
			effects = {"EFF_ZONE_GOD_LINGERING_POWER"},
			color_shown = {0.75, 0.7, 0.7, 1},
			color_obscure = {0.75*0.6, 0.7*0.6, 0.7*0.6, 0.6},
			generator = {
				trap =
				{
					nb_trap = {10, 20}
				}
			},
		},
		[6] = {
			ambient_music = "Driving the Top Down.ogg",
			all_lited = true,
			no_level_connectivity = true,
			generator = {
				map = {
					class = "engine.generator.map.Static",
					map = "zones/embers-wizard-exit",
				},
				actor =
				{
					nb_npc = {0, 0}
				},
				object =
				{
					nb_object = {0, 3}
				}
			},
		},
	},

	post_process = function(level)
		local amountOfStatPotions;
		if level.level < 3 then
			amountOfStatPotions = rng.range(4, 6)
		elseif level.level < 6 then
			amountOfStatPotions = rng.range(2, 5)
		else
			amountOfStatPotions = 0
		end

		while amountOfStatPotions > 0 do
			local statPotion = game.zone:makeEntityByName(level, "object", "SPECIAL_STAT_POTION")
			if statPotion then
				local x, y
				local notAddedYet = true
				while notAddedYet do
					x = rng.range(1, level.map.w)
					y = rng.range(1, level.map.h)
					x, y = util.findFreeGrid(x, y, 5, true, {[level.map.OBJECT]=true})
					if x and y then
						statPotion.no_decay = true
						game.zone:addEntity(level, statPotion, "object", x, y)
						notAddedYet = false
					end
				end
			end
			amountOfStatPotions = amountOfStatPotions - 1
		end

		if level.level == 2 then
			--local vaultuid = rng.range(0, 10000)
			for _, z in ipairs(level.custom_zones) do
				--vaultuid = vaultuid+1
				if z.type == "zonename" then
					for x = z.x1, z.x2 do 
						for y = z.y1, z.y2 do
							game.level.map.attrs(x, y, "zonename", z.subtype)							
							game.level.map.attrs(x, y, "no_decay", true)
							--game.level.map.attrs(x, y, "vault_id", vaultuid)
							game.level.map.attrs(x, y, "no_teleport", true)
						end
					end
				end
			end
		elseif level.level == 5 or rng.range(1, 20) <= level.level then
			local bloodKnife = game.zone:makeEntityByName(level, "object", "GERLYK_BLOODIED_KNIFE")
			if bloodKnife then
				local x, y = util.findFreeGrid(level.default_up.x, level.default_up.y+1, 5, true, {[level.map.OBJECT]=true})
				if x and y and (not level.map.attrs[x+y*level.map.w] or not level.map.attrs[x+y*level.map.w].zonename) then
					bloodKnife.no_decay = true
					game.zone:addEntity(level, bloodKnife, "object", x, y)
				else
					bloodKnife.no_decay = true
					game.zone:addEntity(level, bloodKnife, "object", level.default_down.x, level.default_down.y)
				end
			end
		end
	end,

	on_enter = function(levelNum, old_level, newzone)
		if game.level.level == 6 then
			local bloodKnife = game.zone:makeEntityByName(game.level, "object", "GERLYK_BLOODIED_KNIFE")
			if bloodKnife then
				game.zone:addEntity(game.level, bloodKnife, "object", game.level.default_up.x, game.level.default_up.y)				
			end
		end

		if game.level.level == 6 and not game.player:hasQuest("start-burningcoal"):isCompleted("killed-bone-golem") then
			local playerFireDamage = game.player.melee_project.FIRE or 0
			local playerLightDamage = game.player.melee_project.LIGHT or 0
			local playerHolyDamage = game.player.melee_project.HOLY_LIGHT or 0
			local playerTotalDamage = playerFireDamage+playerLightDamage+playerHolyDamage

			if playerTotalDamage < 20 then
				require("engine.ui.Dialog"):simplePopup("Path to Exit", "The path to the exit is blocked by a gigantic golem made of bones, you guess that at least with your normal attacks you can't kill it right now.")
			end
		end
	end,
}