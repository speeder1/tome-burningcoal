local DamageType = require "engine.DamageType"

load("/data/general/objects/objects-maj-eyal.lua")

newEntity{
	power_source = {unknown=true},
	define_as = "SPECIAL_STAT_POTION",
	type = "potion", subtype="potion",
	name = "Failed Potion", unique=false, unided_name="strange potion", image="object/potion-5x0.png",
	display = "*", color=colors.VIOLET,
	desc = [[A vial of some blue bubbly liquid with no smell or any other distinguishing feature.]],
	cost = 3000,
	quest = 1,
	stacking = true,

	use_simple = { no_npc_use = true, name="drink the potion", use = function(self, who)
		if rng.range(1, 20) < 5 then --20%
			-- poison potion
			game.logPlayer(who, "#00FFFF#You drink potion and feel sick!")
			who:setEffect(who.EFF_POISONED, 5, {src=self, power=100})
			who:setEffect(who.EFF_INSIDIOUS_POISON, 5, {src=self, power=50})
		else
			-- stat potion
			game.logPlayer(who, "#00FFFF#You drink potion and feel transformed!")

			local statToIncrease = nil
			local lowestStat = 1
			local highestStat = 1

			local statCounter = 2
			while statCounter <= 6 do --stat 7 is luck, usually...
				if who.stats[statCounter] > who.stats[highestStat] then highestStat = statCounter end
				if who.stats[statCounter] < who.stats[lowestStat] then lowestStat = statCounter end
				statCounter = statCounter + 1
			end

			if rng.range(1, 20) < 5 then --20%				
				statToIncrease = lowestStat
			elseif rng.range(1, 20) < 5 then
				statToIncrease = highestStat
			end

			if not statToIncrease then statToIncrease = rng.range(1, 6) end
			local statName = who.stats_def[statToIncrease].short_name

			who:incStat(statName, 1)

			
			if statName == "str" then
				game.logPlayer(who, "#00FF00#You feel stronger!")
			elseif statName == "dex" then				
				game.logPlayer(who, "#00FF00#You feel faster!")
			elseif statName == "con" then				
				game.logPlayer(who, "#00FF00#You feel healthier!")
			elseif statName == "cun" then				
				game.logPlayer(who, "#00FF00#You feel smarter!")
			elseif statName == "wil" then				
				game.logPlayer(who, "#00FF00#You feel calmer!")
			elseif statName == "mag" then
				game.logPlayer(who, "#00FF00#You feel arcane energies!")
			end
		end

		return {used=true, id=true, destroy=true}
	end}
}

newEntity{
	power_source = {unknown=true},
	define_as = "GOD_EXTRACT",
	type = "potion", subtype="potion",
	name = "God's Extract", unique=true, unided_name="strange potion", image="object/potion-5x0.png",
	display = "*", color=colors.VIOLET,
	desc = [[A vial of some blue bubbly liquid with no smell or any other distinguishing feature.

	But from what you could read from the notes of the potion brewer, it is extract of some god power]],
	cost = 3000,
	quest = 1,
	
	use_simple = { no_npc_use = true, name="drink the potion.", use = function(self, who)
		game.logPlayer(who, "#00FFFF#You drink the potion and feel forever transformed!")
		game.logPlayer(who, "#FFFF00#You feel blessed!")
		who.inc_stats[who.STAT_LCK] = who.inc_stats[who.STAT_LCK] + 20
		who:onStatChange(who.STAT_LCK, 20)
		game.logPlayer(who, "#FF0000#You feel healthier!")
		who.max_life = who.max_life + 100
		who.life_regen = who.life_regen + 2
		game.logPlayer(who, "#0000FF#You feel more experienced!")
		who.unused_talents = who.unused_talents + 3
		who.unused_generics = who.unused_generics + 7

		return {used=true, id=true, destroy=true}
	end}
}

newEntity{ base = "BASE_KNIFE",
	define_as = "GERLYK_BLOODIED_KNIFE",
	power_source = {unknown=true},
	unique = true, plot = true,
	name = "God's Blood", image = "object/artifact/dagger_steel_with_blood.png",
	level_range = {10, 20},
	desc = [[This dagger seems to be formed of pure shadows, with a strange miasma surrounding it.]],
	require = { stat = { dex=10 }, },
	cost = 100,
	material_level = 2,
	rarity = false,
	combat = {
		dam = resolvers.rngavg(10,14),
		apr = 6,
		physcrit = 5,
		dammod = {dex=0.5,str=0.5},
		convert_damage = {
			[DamageType.HOLY_LIGHT] = 100,
		},
	},
	
	use_simple = { no_npc_use = true, name="stab yourself deeply", use = function(self, who)
		game.logPlayer(who, "#00FFFF#You stab yourself to the hilt in the chest and feel forever transformed!")
		game.logPlayer(who, "#FFFF00#You feel blessed!")
		who.inc_stats[who.STAT_LCK] = who.inc_stats[who.STAT_LCK] + 20
		who:onStatChange(who.STAT_LCK, 20)
		game.logPlayer(who, "#FF0000#You feel healthier!")
		who.max_life = who.max_life + 100
		game.logPlayer(who, "#0000FF#You feel more experienced!")
		who.unused_talents = who.unused_talents + 1
		who.unused_generics = who.unused_generics + 1
		
		local newDagger = game.zone:makeEntity(game.level, "object", {name="steel dagger", ignore_material_restriction=true, ego_filter={ego_chance=-1000}})
		if newDagger then			
			newDagger:identify(true)
			self:replaceWith(newDagger)
			who:sortInven()
			game.logPlayer(who, "The blood on the knife was wiped clean and absorbed into you!")

			if who:knowTalentType("human/burningcoal") then			
				game.logPlayer(who, "#00FF00#You gain more mastery over your godly powers!")
				who:setTalentTypeMastery("human/burningcoal", who:getTalentTypeMastery("human/burningcoal") + 0.1)
			else
				who:learnTalentType("human/burningcoal", true)
			end

			who:learnTalent(who.T_GODLY_BONES, true, 1, {no_unlearn=true})
			if not who.__show_special_talents then who.__show_special_talents = {} end
			who.__show_special_talents[who.T_GODLY_BONES] = true

			game.party:learnLore("embers-of-creation-godly-bones")
			game.player:setQuestStatus("start-burningcoal", engine.Quest.COMPLETED, "absorbed-gerlyk-blood")
		else
			game.logPlayer(who, "The dagger is gone!")
		end

		return {used=true, id=true, destroy=true}
	end}
}