load("/data/general/npcs/rodent.lua", rarity(0))
load("/data/general/npcs/vermin.lua", rarity(0))
load("/data/general/npcs/molds.lua", rarity(1))
load("/data/general/npcs/bone-giant.lua", rarity(5))
load("/data/general/npcs/construct.lua", rarity(3))
load("/data/general/npcs/skeleton.lua", rarity(2))
load("/data/general/npcs/ghoul.lua", rarity(3))
load("/data/general/npcs/wight.lua", rarity(3))
load("/data/general/npcs/vampire.lua", rarity(6))
load("/data/general/npcs/lich.lua", rarity(9))

load("/data/general/npcs/all.lua", rarity(4, 35))

local Talents = require("engine.interface.ActorTalents")

newEntity{ define_as = "MAD_SCIENTIST",
	allow_infinite_dungeon = true,
	type = "humanoid", subtype = "halfling", unique = true,
	name = "Mad Scientist",
	display = "p", color=colors.VIOLET, female = false,
	image = "player/halfling_male.png",
	moddable_tile = "halfling_#sex#",	
	desc = [[This alchemist is clearly mad, it is unclear if he owns the place or arrived later though.]],
	killer_message = "and used in crazy experiments",
	level_range = {7, 7}, exp_worth = 2,
	max_life = 300, life_rating = 15, fixed_rating = true,
	rank = 4,
	tier1 = true,
	size_category = 2,
	infravision = 10,
	stats = { str=12, dex=16, cun=14, mag=25, con=16 },
	instakill_immune = 1,
	move_others=true,

	body = { INVEN = 10, QS_MAINHAND = 1, QS_OFFHAND = 1, MAINHAND = 1, OFFHAND = 1, FINGER = 2, NECK = 1, LITE = 1, BODY = 1, HEAD = 1, CLOAK = 1, HANDS = 1, BELT = 1, FEET = 1, TOOL = 1, QUIVER = 1, QS_QUIVER = 1, GEM = 1 },
	equipment = resolvers.equip{ 
		{type="weapon", subtype="staff", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="armor", subtype="cloth", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="armor", subtype="cloak", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="armor", subtype="hands", base="BASE_GLOVES", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="armor", subtype="feet", base="BASE_LEATHER_BOOT", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="armor", subtype="belt", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="armor", subtype="head", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="jewelry", subtype="ring", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="jewelry", subtype="ring", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="jewelry", subtype="amulet", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="charm", subtype="torque", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
		{type="lite", subtype="lite", forbid_power_source={antimagic=true}, force_drop=true, tome_drops="boss", autoreq=true},
	},		
	resolvers.drops{chance=100, nb=1, {defined="GOD_EXTRACT"} },	

	resolvers.talents{
		[Talents.T_CREATE_ALCHEMIST_GEMS] = 1,
		[Talents.T_GOLEM_POWER] = 3,
		[Talents.T_GOLEM_RESILIENCE] = 3,
		[Talents.T_REFIT_GOLEM] = 3,
		[Talents.T_THROW_BOMB] = 1,
		[Talents.T_ACID_INFUSION] = 2,
		[Talents.T_ALCHEMIST_PROTECTION] = 2,
		[Talents.T_CAUSTIC_GOLEM] = 1,
		[Talents.T_CHANNEL_STAFF] = 3,
		[Talents.T_STAFF_MASTERY] = 4,
		[Talents.T_HALFLING_LUCK] = 5,
		[Talents.T_DUCK_AND_DODGE] = 1,		
	},
	
	resolvers.inventorybirth{{type="gem",}, {type="gem",}, {type="gem",}, {type="gem",}, {type="gem",}},

	resolvers.inscription("INFUSION:_REGENERATION", {cooldown=10, dur=5, heal=150}),
	resolvers.inscription("INFUSION:_WILD", {cooldown=12, what={physical=true}, dur=4, power=14}),

	resolvers.generic(function(self) self:birth_create_alchemist_golem() end),
	innate_alchemy_golem = true,
	birth_create_alchemist_golem = function(self)				
		if not self.alchemy_golem then
			-- Make and wield some alchemist gems
			local t = self:getTalentFromId(self.T_CREATE_ALCHEMIST_GEMS)
			local gem = t.make_gem(self, t, "GEM_AGATE")
			self:wearObject(gem, true, true)
			self:sortInven()
			
			-- Invoke the golem
			local t = self:getTalentFromId(self.T_REFIT_GOLEM)
			t.invoke_golem(self, t)
		end
		
		
	end,

	NPCmakeGemsTalent = function(self)
		local sourceGem = self:findInInventoryBy(self.inven[self.INVEN_INVEN], "type", "gem")
		if not sourceGem then return end

		local t = self:getTalentFromId(self.T_CREATE_ALCHEMIST_GEMS)
		local gem = t.make_gem(self, t, sourceGem.define_as)
		if not gem then return end		
		self:removeObject(self.INVEN_INVEN, sourceGem)		
		game.logSeen(self, "%s creates: %s", self.name, gem:getName{do_color=true, do_count=true})
		self:sortInven()
		self:addObject(self.INVEN_INVEN, gem)
		self:sortInven()

		game:playSoundNear(self, "talents/arcane")
		self:useEnergy()
		self.mana = self.mana - 30
	end,

	NPCWieldGem = function(self, gem)
		self:wearObject(gem, true, true)
		self:sortInven()
		game.logSeen(self, "%s wields: %s", self.name, gem:getName{do_color=true, do_count=true})
		self:useEnergy()
	end,


	ressurectGolemNPC = function(self)
		if not self.golemRefitTimer then
			self.golemRefitTimer = 20
		end

		if self.golemRefitTimer > 0 then
			self:useEnergy()
			self.golemRefitTimer = self.golemRefitTimer - 1
			game.logSeen(self, "#YELLOW#%s is trying to fix his Golem.",self.name)
			return
		end

		self.golemRefitTimer = 20		
		game.logSeen(self, "#RED#%s fixed his Golem, it looks reinforced!",self.name)

		for i = 1, 15 do self:removeObject(self:getInven("QUIVER"), 1) end

		self.alchemy_golem.dead = nil
		if not self.alchemy_golem.deaths then self.alchemy_golem.deaths = 1 else self.alchemy_golem.deaths = self.alchemy_golem.deaths + 1 end
		self.alchemy_golem.max_life = self.alchemy_golem.max_life + self.alchemy_golem.deaths*self.alchemy_golem.deaths*50
		self.alchemy_golem.life_regen = self.alchemy_golem.life_regen + self.alchemy_golem.deaths
		self.alchemy_golem.exp_worth = self.alchemy_golem.deaths
		if self.alchemy_golem.life <= 0 then self.alchemy_golem.life = self.alchemy_golem.max_life / 3 end

		local x, y = util.findFreeGrid(self.x, self.y, 5, true, {[game.level.map.ACTOR]=true})
		if not x then
			x = self.x
			y = self.y			
		end

		game.zone:addEntity(game.level, self.alchemy_golem, "actor", x, y)
		--self.alchemy_golem:setTarget(nil)
		self.alchemy_golem.ai_state.tactic_leash_anchor = self
		self.alchemy_golem:removeAllEffects()
		self:startTalentCooldown(self.T_REFIT_GOLEM)
		self:useEnergy()
		self.mana = self.mana-10
	end,

	on_added_to_level = function(self, level, x, y)
		local param_add_trees={["race/halfling"]={true, 0}, ["spell/explosives"]={true, 0.3}, ["spell/golemancy"]={true, 0.3}, ["spell/stone-alchemy"]={true, 0.3},	["spell/fire-alchemy"]={true, 0.3}, ["spell/acid-alchemy"]={true, 0.5}, ["spell/frost-alchemy"]={true, 0.3}, ["spell/staff-combat"]={true, 0.3}, ["cunning/survival"]={true, -0.1}}

		local data = {force_classes = {Alchemist = true}, nb_classes=0, check_talents_level=true, auto_sustain=true, forbid_equip=true, add_trees=param_add_trees}
		game.state:applyRandomClass(self, data, false)

		self:check("birth_create_alchemist_golem")
		for tid, lev in pairs(self.learn_tids) do
			if self:getTalentLevelRaw(tid) < lev then
				self:learnTalent(tid, true, lev - self:getTalentLevelRaw(tid))
			end
		end
		
		self.alchemy_golem.exp_worth = 1

		self.alchemy_golem.on_act = function(self)			
			if self.summoner.dead then
				local target = {type="ball", range=0, friendlyfire=true, radius=4}
				local damage = self.max_life/5+self.life
				self:project(target, self.x, self.y, DamageType.FIREKNOCKBACK, damage)
				self:project(target, self.x, self.y, DamageType.PHYSICAL, damage/2)
				game.logSeen(self, "#RED#%s selfdestructed!",self.name)
				game.level.map:particleEmitter(self.x, self.y, target.radius, "ball_fire", {radius=target.radius})
				game:playSoundNear(self, "talents/fireflash")
				self:die(self)
			end
		end,

		game.zone:addEntity(level, self.alchemy_golem, "actor", x, y-3)
		--self.alchemy_golem.x = self.x
	end,

	on_act = function(self)
		if not self:isTalentActive(self.T_ACID_INFUSION) and self.mana > 80 and not self:isTalentCoolingDown(self.T_ACID_INFUSION) then
			self:useTalent(self.T_ACID_INFUSION)
			return
		end


		local ammo = self:hasAlchemistWeapon()
		if not ammo or ammo:getNumber() < 15 then
			local gem = self:findInInventoryBy(self.inven[self.INVEN_INVEN], "type", "alchemist-gem")
			if not gem or not gem.stacked then
				if self:isTalentCoolingDown(self.T_CREATE_ALCHEMIST_GEMS) then
					return
				end
				self:NPCmakeGemsTalent()				
			else
				self:NPCWieldGem(gem)
			end			
			return
		end

		if self.alchemy_golem then
			local t = self:getTalentFromId(self.T_REFIT_GOLEM)			
			local golemHealAmount = t.getHeal(self, t)
			
			if self.alchemy_golem.dead then												
				if not ammo or ammo:getNumber() < 15 then
					return
				end

				if self:isTalentCoolingDown(self.T_REFIT_GOLEM) then
					return
				end

				self:ressurectGolemNPC()
				return

			elseif not self:isTalentCoolingDown(self.T_REFIT_GOLEM) and self.alchemy_golem.life <= self.alchemy_golem.max_life*0.4 or self.alchemy_golem.life <= self.alchemy_golem.max_life-golemHealAmount then

				if not ammo or ammo:getNumber() < 2 then
					return
				end	
				
				self:useTalent(self.T_REFIT_GOLEM)
				return

			elseif not self:isTalentCoolingDown(self.T_THROW_BOMB) and ammo:getNumber() > 2 and self.mana > self.max_mana*0.2 and self.ai_target and self.ai_target.actor and self.ai_target.actor.__CLASSNAME == "mod.class.Player" then
				self:useTalent(self.T_THROW_BOMB)
				return
			end
		end
	end,


	resolvers.inscriptions(1, {"shielding rune", "speed rune"}),
	resolvers.inscriptions(1, {"manasurge rune"}),	

	autolevel = "caster",
	ai = "tactical", ai_state = { talent_in=1, ai_move="move_astar", },	
	ai_tactic = resolvers.tactic"ranged",

	on_die = function(self, who)
		game.party:learnLore("embers-of-creation-mad-scientist")
		game.logPlayer(game.player, "The halfling scientist had with him a key, and a journal.")
	end,
}

newEntity{ base = "BASE_NPC_BONE_GIANT", define_as = "BONE_GOLEM_BOSS",
	name = "Mobile Wall, The Bone Golem", color=colors.RED,
	desc = [[A towering creature, made from the bones of hundreds of dead bodies, rune-etched and infused with hateful sorceries.]],
	type = "construct", subtype = "undead", unique = true,
	allow_infinite_dungeon = true,
	resolvers.nice_tile{image="invis.png", add_mos = {{image="npc/undead_giant_runed_bone_giant.png", display_h=2, display_y=-1}}},
	killer_message = "and kept attacking the body like an automaton until nothing remained to be attacked",
	level_range = {15, nil}, exp_worth = 2,
	max_life = 600, life_rating = 16, fixed_rating = true,
	instakill_immune = 1,
	move_others=true,
	life_regen = 20,	
	rank = 5,
	size_category = 6,
	ai = "tactical",
	max_life = resolvers.rngavg(100,120),
	combat_armor = 20, combat_def = 40,
	melee_project = {[DamageType.BLIGHT]=resolvers.mbonus(15, 15)},
	autolevel = "warriormage",
	resists = {
		[DamageType.PHYSICAL]=100,
		[DamageType.FIRE]=-20, [DamageType.COLD]=100, [DamageType.ACID]=100, [DamageType.LIGHTNING]=100,
		[DamageType.LIGHT]=-20, [DamageType.DARKNESS]=100,
		[DamageType.NATURE]=100, [DamageType.BLIGHT]=100,
		[DamageType.TEMPORAL]=100,
		[DamageType.MIND]=100,
		[DamageType.ARCANE]=100,
	},
	resolvers.talents{
		[Talents.T_BONE_ARMOUR]={base=5, every=10, max=7},
		[Talents.T_STUN]={base=3, every=10, max=5},
		[Talents.T_SKELETON_REASSEMBLE]=5,
		[Talents.T_ARCANE_POWER]={base=4, every=5, max = 8},
		[Talents.T_MANATHRUST]={base=4, every=5, max = 10},
		[Talents.T_MANAFLOW]={base=5, every=5, max = 10},
		[Talents.T_STRIKE]={base=4, every=5, max = 12},
		[Talents.T_INNER_POWER]={base=4, every=5, max = 10},
		[Talents.T_EARTHEN_MISSILES]={base=5, every=5, max = 10},
	},
	resolvers.sustains_at_birth(),
	resolvers.drops{chance=100, nb=2, {tome_drops="boss"} },

	on_die = function(self, who)
		game.player:setQuestStatus("start-burningcoal", engine.Quest.COMPLETED, "killed-bone-golem")
	end,
}