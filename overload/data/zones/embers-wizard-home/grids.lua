load("/data/general/grids/basic.lua")

newEntity{
	define_as = "SIGN_POST_LAVA",
	name = "warning sign", image = "terrain/marble_floor.png",
	display = '_', color=colors.GREEN, back_color=colors.DARK_GREY,
	add_displays = {class.new{image="terrain/signpost09.png"}},
	always_remember = true,
	notice = true,
	lore = "embers-lava-sign",
	on_move = function(self, x, y, who)
		if not who.player then return end
		game.party:learnLore(self.lore)
	end,
}

newEntity{
	define_as = "SIGN_POST_VAULT",
	name = "warning sign", image = "terrain/marble_floor.png",
	display = '_', color=colors.GREEN, back_color=colors.DARK_GREY,
	add_displays = {class.new{image="terrain/signpost09.png"}},
	always_remember = true,
	notice = true,
	lore = "embers-vault-sign",
	on_move = function(self, x, y, who)
		if not who.player then return end
		game.party:learnLore(self.lore)
	end,
}

newEntity{
	define_as = "EXIT_TO_MAZE",
	name = "teleporting circle to the defensive maze", image = "terrain/marble_floor.png", add_displays = {class.new{image="terrain/maze_teleport.png"}},
	display = '<', color_r=255, color_g=0, color_b=255,
	notice = true,
	change_level = 2, change_zone = "embers-wizard-maze", force_down = true,
}

newEntity{
	define_as = "TELEPORTER_LAVA_DUNGEON",
	name = "teleporting circle to some fiery place", image = "terrain/marble_floor.png", add_displays = {class.new{image="terrain/maze_teleport.png"}},
	display = '>', color_r=255, color_g=0, color_b=255,
	notice = true,
	change_level = 1, change_zone = "embers-lava-dungeon",
}

newEntity{
	define_as = "KEY_LOCKED_DOOR",
	type = "wall", subtype = "floor",
	name = "locked door", image = "terrain/granite_door1.png",
	display = '+', color_r=238, color_g=154, color_b=77, back_color=colors.DARK_UMBER,
	nice_tiler = { method="door3d", north_south="KEY_LOCKED_DOOR_VERT", west_east="KEY_LOCKED_DOOR_HORZ" },
	notice = true,
	always_remember = true,
	block_sight = true,
	block_sense = true,
	block_esp = true,	
	block_move = function(self, x, y, e, act)
		if act and e.player then
			if game.party:knownLore("embers-of-creation-mad-scientist") then
				self.is_door = true
				self.block_move = nil
				game.level.map(x, y, engine.Map.TERRAIN, game.zone.grid_list[self.door_opened])
				game.logPlayer(game.player, "You unlock the door with the key you found on the halfling alchemist.")
			else
				engine.ui.Dialog:simplePopup("Locked Door", "This door has an hybrid lock, magical and mechanical, you have no idea how to open it without the proper key.")
			end
		end
		return true
	end,
	door_opened = "DOOR_OPEN",
}

newEntity{ base = "KEY_LOCKED_DOOR", define_as = "KEY_LOCKED_DOOR_HORZ", z=3, image = "terrain/granite_door1.png", add_displays = {class.new{image="terrain/granite_wall3.png", z=18, display_y=-1}}, door_opened = "DOOR_HORIZ_OPEN"}
newEntity{ base = "KEY_LOCKED_DOOR", define_as = "KEY_LOCKED_DOOR_VERT", image = "terrain/marble_floor.png", add_displays = {class.new{image="terrain/granite_door1_vert.png", z=17}, class.new{image="terrain/granite_door1_vert_north.png", z=18, display_y=-1}}, door_opened = "DOOR_OPEN_VERT"}

newEntity{
	define_as = "TELEPORT_OUT",
	name = "teleportation circle to the surface", image = "terrain/marble_floor.png", add_displays = {class.new{image="terrain/maze_teleport.png"}},
	display = '>', color_r=255, color_g=0, color_b=255,
	notice = true, show_tooltip = true,
	change_level = 1, change_zone = "wilderness", change_level_check = function()
		require("engine.ui.Dialog"):yesnoPopup("Leaving the Dungeon", "You aren't sure you can come back if you leave, are you certain you won't abandon any unfinished business or unlooted treasure?", function(ret)
			if ret then 
				game:changeLevel(1, "wilderness")
				local burningCoalQuest = game.player:hasQuest("start-burningcoal")
				if not burningCoalQuest then return end
				if burningCoalQuest:allDone() then
					game.player:setQuestStatus("start-burningcoal", engine.Quest.DONE)
				else
					game.player:setQuestStatus("start-burningcoal", engine.Quest.FAILED)
				end

				game.player:grantQuest("tier1-burningcoal")
			end
		end)
		return true
	end
}

newEntity { base = "FLOOR", define_as = "BIG_RUNE_FLOOR", add_displays = {class.new{z=3, image="terrain/bigrune.png", display_y = -3, display_x = -3, display_w=7, display_h=7}}}

newEntity { base = "FLOOR", block_move = true, define_as = "DECO_EQUIPMENT", nice_tiler = { method="replace", base={"DECO_EQUIPMENT", 100, 1, 4}}}
newEntity { base = "FLOOR", block_move = true, define_as = "DECO_EQUIPMENT1", add_displays = {class.new{z=3, image="terrain/ruins/floor_vat_broken_deco_01.png", display_y=-1, display_h=2}}, name = "broken vat"}
newEntity { base = "FLOOR", block_move = true, define_as = "DECO_EQUIPMENT2", add_displays = {class.new{z=3, image="terrain/ruins/floor_vat_broken_deco_02.png"}}, name = "broken vat"}
newEntity { base = "FLOOR", block_move = true, define_as = "DECO_EQUIPMENT3", add_displays = {class.new{z=3, image="terrain/ruins/vat_broken_01.png", display_y=-1, display_h=2}}, name = "broken vat"}
newEntity { base = "FLOOR", block_move = true, define_as = "DECO_EQUIPMENT4", add_displays = {class.new{z=3, image="terrain/ruins/vat_broken_02.png", display_y=-1, display_h=2}}, name = "broken vat"}

newEntity { base = "FLOOR", define_as = "RED_CARPET", image = "terrain/red_carpet5.png"}

local count = 1
while count < 10 do
	newEntity { base = "FLOOR", define_as = "RED_CARPET" .. count, add_displays = {class.new{z=3, image="terrain/red_carpet"..count..".png"}}}
	count = count + 1
end