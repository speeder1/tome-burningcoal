-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2016 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

load("/data/general/grids/basic.lua")

newEntity{
	define_as = "EXIT_TO_WIZARD_HOUSE",
	name = "teleporting circle to a sorcerer fortress", image = "terrain/maze_floor.png", add_displays = {class.new{image="terrain/maze_teleport.png"}},
	display = '>', color_r=255, color_g=0, color_b=255,
	notice = true,
	change_level = 1, change_zone = "embers-wizard-home",
}

newEntity{
	define_as = "BACK_INTO_CAVE", image = "terrain/maze_floor.png", add_mos = {{image="terrain/stair_up.png"}},
	type = "floor", subtype = "floor",
	name = "previous level",
	display = '<', color_r=255, color_g=255, color_b=0,
	notice = true,
	change_level = 2, change_zone = "embers-cave", force_down = true,
}

newEntity{
	define_as = "OLD_UP", image = "terrain/maze_floor.png", add_mos = {{image="terrain/stair_up.png"}},
	type = "floor", subtype = "floor",
	name = "previous level",
	display = '<', color_r=255, color_g=255, color_b=0,
	notice = true,
	always_remember = true,
	change_level = -1,
}