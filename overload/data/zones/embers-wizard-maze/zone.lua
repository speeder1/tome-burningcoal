-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2016 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

return {
	name = "Embers of Creation - Defensive Maze",
	level_range = {2, 6},
	level_scheme = "player",
	max_level = 2,
	decay = {300, 800},
	actor_adjust_level = function(zone, level, e) return zone.base_level + e:getRankLevelAdjust() + level.level-1 + rng.range(-1,2) end,
	width = 34, height = 34,
--	all_remembered = true,
--	all_lited = true,
	persistent = "zone",
	ambient_music = {"The Ancients.ogg","weather/dungeon_base.ogg"},
	min_material_level = 1,
	max_material_level = 2,
	events_by_level = true,
	generator =  {
		map = {
			class = "engine.generator.map.Maze",
			up = "OLD_UP",
			down = "DOWN",
			wall = "OLD_WALL",
			floor = "OLD_FLOOR",
			widen_w = 2, widen_h = 2,
		},
		actor = {
			class = "mod.class.generator.actor.Random",
			nb_npc = {0, 20},
		},
		object = {
			class = "engine.generator.object.Random",
			nb_object = {0, 8},
		},
		trap = {
			class = "engine.generator.trap.Random",
			nb_trap = {3, 12},
		},
	},
	levels =
	{		
		[1] = {
			persistent = false,
			generator = 
			{ 
				map = 
				{					
					up = "BACK_INTO_CAVE",
				}, 
			},
		},
		[2] = {
			persistent = false,
			generator = 
			{ 
				map = 
				{
					force_last_stair = true,
					down = "EXIT_TO_WIZARD_HOUSE",
				}, 
			},
		},
	},

	post_process = function(level)
		game.state:makeAmbientSounds(level, {
			dungeon2={ chance=250, volume_mod=1, pitch=1, random_pos={rad=10}, files={"ambient/dungeon/dungeon1","ambient/dungeon/dungeon2","ambient/dungeon/dungeon3","ambient/dungeon/dungeon4","ambient/dungeon/dungeon5"}},
		})
	end,
}