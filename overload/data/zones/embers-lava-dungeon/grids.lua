-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2016 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

load("/data/general/grids/basic.lua")
load("/data/general/grids/mountain.lua")
load("/data/general/grids/lava.lua", function(e) if e.define_as == "LAVA_FLOOR" then e.grow = "CRYSTAL_WALL" end end)

grow = "WALL_ON_LAVA",

newEntity{
	define_as = "TELEPORTER_TO_WIZARD_HOME",
	name = "teleporting circle to the sorcerer's fortress", image = "terrain/lava_floor.png", add_displays = {class.new{image="terrain/maze_teleport.png"}},
	display = '<', color_r=255, color_g=0, color_b=255,
	notice = true,
	change_level = 1, change_zone = "embers-wizard-home", change_zone_auto_stairs = true,
}

newEntity{
	define_as = "LIQUID_LAVA",
	type = "floor", subtype = "molten_lava",
	name = "molten lava", image = "terrain/lava/molten_lava_5_01.png",
	display = '~', color=colors.LIGHT_RED, back_color=colors.RED,
	shader = "lava",
	mindam = resolvers.mbonus(30, 50),
	maxdam = resolvers.mbonus(50, 200),	
	on_stand = function(self, x, y, who)
		local DT = engine.DamageType
		local dam = DT:get(DT.FIRE).projector(self, x, y, DT.FIRE, rng.range(self.mindam, self.maxdam))
		self.x, self.y = x, y
		if dam > 0 and who.player then self:logCombat(who, "#Source# burns #Target#!") end
	end,
	nice_editer = molten_lava_editer,
	nice_tiler = { method="replace", base={"LIQUID_LAVA", 10, 2, 5} },
}
for i = 2, 5 do newEntity{ base="LIQUID_LAVA", define_as = "LIQUID_LAVA"..i, image = "terrain/lava/molten_lava_5_0"..i..".png"} end

newEntity{
	define_as = "WALL_ON_LAVA",
	type = "wall", subtype = "lava_floor",
	name = "wall", image = "terrain/granite_wall1.png",
	display = '#', color_r=255, color_g=255, color_b=255, back_color=colors.GREY, tint_r=255, tint_g=60, tint_b=10,
	z = 3,
	nice_tiler = { method="wall3d", inner={"WALL_ON_LAVA", 100, 1, 5}, north={"WALL_ON_LAVA_NORTH", 100, 1, 5}, south={"WALL_ON_LAVA_SOUTH", 10, 1, 17}, north_south="WALL_ON_LAVA_NORTH_SOUTH", small_pillar="WALL_ON_LAVA_SMALL_PILLAR", pillar_2="WALL_ON_LAVA_PILLAR_2", pillar_8={"WALL_ON_LAVA_PILLAR_8", 100, 1, 5}, pillar_4="WALL_ON_LAVA_PILLAR_4", pillar_6="WALL_ON_LAVA_PILLAR_6" },
	always_remember = true,
	does_block_move = true,
	can_pass = {pass_wall=1},
	block_sight = true,
	air_level = -20,
	dig = "LAVA_FLOOR",
}
for i = 1, 5 do
	newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA"..i, image = "terrain/granite_wall1_"..i..".png", z = 3}
	newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA_NORTH"..i, image = "terrain/granite_wall1_"..i..".png", z = 3, add_displays = {class.new{image="terrain/granite_wall3.png", z=18, display_y=-1}}}
	newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA_PILLAR_8"..i, image = "terrain/granite_wall1_"..i..".png", z = 3, add_displays = {class.new{image="terrain/granite_wall_pillar_8.png", z=18, display_y=-1}}}
end
newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA_NORTH_SOUTH", image = "terrain/granite_wall2.png", z = 3, add_displays = {class.new{image="terrain/granite_wall3.png", z=18, display_y=-1}}}
newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA_SOUTH", image = "terrain/granite_wall2.png", z = 3}
for i = 1, 17 do newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA_SOUTH"..i, image = "terrain/granite_wall2_"..i..".png", z = 3} end
newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA_SMALL_PILLAR", image = "terrain/lava_floor.png", z=1, add_displays = {class.new{image="terrain/granite_wall_pillar_small.png",z=3}, class.new{image="terrain/granite_wall_pillar_small_top.png", z=18, display_y=-1}}}
newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA_PILLAR_6", image = "terrain/lava_floor.png", z=1, add_displays = {class.new{image="terrain/granite_wall_pillar_3.png",z=3}, class.new{image="terrain/granite_wall_pillar_9.png", z=18, display_y=-1}}}
newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA_PILLAR_4", image = "terrain/lava_floor.png", z=1, add_displays = {class.new{image="terrain/granite_wall_pillar_1.png",z=3}, class.new{image="terrain/granite_wall_pillar_7.png", z=18, display_y=-1}}}
newEntity{ base = "WALL_ON_LAVA", define_as = "WALL_ON_LAVA_PILLAR_2", image = "terrain/lava_floor.png", z=1, add_displays = {class.new{image="terrain/granite_wall_pillar_2.png",z=3}}}

--- Generate sub entities to make nice crystals, same as trees but change tint
function class:makeLavaCrystals(base, max)
	local function makeTree(nb, z)
		local inb = 4 - nb
		local r = rng.range(60, 100)
		local g = rng.range(30, 70)
		local b = rng.range(1, 50)
		local maxcol = math.max(r, g, b)
		r = r / maxcol
		g = g / maxcol
		b = b / maxcol
		return engine.Entity.new{
			z = z,
			display_scale = rng.float(0.5 + inb / 6, 1.3),
			display_x = rng.float(-1 / 3 * nb / 3, 1 / 3 * nb / 3),
			display_y = rng.float(-1 / 3 * nb / 3, 1 / 3 * nb / 3),
			display_on_seen = true,
			display_on_remember = true,
			tint_r = r,
			tint_g = g,
			tint_b = b,
			image = (base or "terrain/red_crystal_alpha")..rng.range(1,max or 6)..".png",
		}
	end

	local v = rng.range(0, 100)
	local tbl
	if v < 33 then
		tbl = { makeTree(3, 16), makeTree(3, 17), makeTree(3, 18), }
	elseif v < 66 then
		tbl = { makeTree(2, 16), makeTree(2, 17), }
	else
		tbl = { makeTree(1, 16), }
	end
	table.sort(tbl, function(a,b) return a.display_scale < b.display_scale end)
	for i = 1, #tbl do tbl[i].z = 16 + i - 1 end
	return tbl
end

for i = 1, 30 do
	newEntity{
		define_as = "CRYSTAL_WALL"..i,
		type = "wall", subtype = "underground",
		name = "crystals",
		image = "terrain/lava_floor.png",
		add_displays = class:makeLavaCrystals("terrain/red_crystal_alpha", 6),
		shader = "lava",
		display = '#', color=colors.LIGHT_RED, back_color=colors.RED,
		always_remember = true,
		can_pass = {pass_wall=1},
		does_block_move = true,
		block_sight = true,
		dig = "LAVA_FLOOR",
	}
end