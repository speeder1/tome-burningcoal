-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2016 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

return {
	name = "Embers of Creation - Lava Realm",
	level_range = {6, 9},
	level_scheme = "player",
	max_level = 1,
	decay = {300, 800},
	actor_adjust_level = function(zone, level, e) return zone.base_level + e:getRankLevelAdjust() + level.level-1 + rng.range(-1,2) end,
	width = 100, height = 100,
--	all_remembered = true,
	all_lited = true,
	day_night = false,
	persistent = "zone",
	ambient_music = "Challenge.ogg",
	min_material_level = 1,
	max_material_level = 2,
	--no_level_connectivity = true,
	generator =  {
		map = {
			class = "mod.class.generator.map.Caldera",
			trees_max_percent = 80,
			trees_sqrt_percent = 30,
			mountain = "LAVA_WALL",
			tree = function() return "CRYSTAL_WALL" .. rng.range(1, 30) end,
			grass = function() return rng.percent(20) and "LIQUID_LAVA" or "LAVA_FLOOR" end,
			water = "LIQUID_LAVA",
			up = "TELEPORTER_TO_WIZARD_HOME",
			down = "LAVA_FLOOR", down_center = true,
		},
		actor = {
			class = "mod.class.generator.actor.Random",
			nb_npc = {40, 70},			
		},
		object = {
			class = "engine.generator.object.Random",
			nb_object = {8, 15},
		},		
	},

	post_process = function(level)
		
		local crystal = game.zone:makeEntityByName(level, "object", "CRYSTAL_GOD_ESSENCE")
		if crystal then
			local x, y = util.findFreeGrid(level.default_down.x, level.default_down.y, 5, true, {[level.map.OBJECT]=true})
			if x and y then
				crystal.no_decay = true
				game.zone:addEntity(level, crystal, "object", x, y)				
			end
		end

		game.state:makeWeather(level, 6, {max_nb=7, chance=1, dir=120, speed={0.1, 0.9}, alpha={0.2, 0.4}, particle_name="weather/grey_cloud_%02d"})
	end,

	on_enter = function(lev)
		if lev == 1 and not game.level.data.warned then
			game.level.data.warned = true
			require("engine.ui.Dialog"):simpleLongPopup("BOOM!", "There is a searing heat here.", 400)
		end
	end,
}
