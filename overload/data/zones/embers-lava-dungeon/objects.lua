-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2016 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

load("/data/general/objects/objects-maj-eyal.lua")

newEntity{
	power_source = {unknown=true},
	define_as = "CRYSTAL_GOD_ESSENCE",
	type = "corpse", subtype = "essence", image = "object/artifact/god_crystal.png",
	name = "Crystalized God Essence", unique=true, unided_name="weird crystal", plot=true,
	display = "*", color=colors.VIOLET,
	desc = [[This is some god lingering aura concentrated in one spot, it became some sort of crystal.

	The object emits some kind of mental pulse that invites to press it against the heart.]],
	cost = 3000,
	quest = 1,

	use_simple = { no_npc_use = true, name="press against the chest", use = function(self, who)
		game.logPlayer(who, "#00FFFF#You presses the crystal against your chest, and suddenly it gets inside your body and merges with you!")
		
		-- Godly Body talent gives 1 extra of each talent type every 3 levels
		who.unused_talents = who.unused_talents + math.floor(who.level / 3) -- "backapply" levelup bonus to talents
		who.unused_generics = who.unused_generics + math.floor(who.level / 3) -- "backapply" levelup bonus to talents

		who.unused_talents_types = who.unused_talents_types + 1 -- give one extra catpoint

		who.life_rating = who.life_rating + 3 -- give extra life per level
		who.max_life = who.max_life + math.max(who:getRankLifeAdjust(3), 1)*who.level -- "backapply" extra life per level

		-- give extra stats per level
		if not who.stats_per_level then who.stats_per_level = 3 end 
		who.stats_per_level = who.stats_per_level + 0.5
		who.unused_stats = who.unused_stats + who.level * 0.5 -- "backapply" levelup bonus to stats

		game.logPlayer(who, "You have %d stat point(s) to spend. Press p to use them.", who.unused_stats)
		game.logPlayer(who, "You have %d class talent point(s) to spend. Press p to use them.", who.unused_talents)
		game.logPlayer(who, "You have %d generic talent point(s) to spend. Press p to use them.", who.unused_generics)

		if who:knowTalentType("human/burningcoal") then			
			game.logPlayer(who, "#00FF00#You gain more mastery over your godly powers!")
			who:setTalentTypeMastery("human/burningcoal", who:getTalentTypeMastery("human/burningcoal") + 0.1)
		else
			who:learnTalentType("human/burningcoal", true)
		end

		who:learnTalent(who.T_GODLY_BODY, true, 1, {no_unlearn=true})
		if not who.__show_special_talents then who.__show_special_talents = {} end
		who.__show_special_talents[who.T_GODLY_BODY] = true

		game.logPlayer(who, "#FFFF00#You feel blessed!")
		who.inc_stats[who.STAT_LCK] = who.inc_stats[who.STAT_LCK] + 20
		who:onStatChange(who.STAT_LCK, 20)

		who.resists[DamageType.COLD] = ( who.resists[DamageType.COLD] or 0 ) - 25
		who.resists[DamageType.DARKNESS] = ( who.resists[DamageType.DARKNESS] or 0 ) - 25
		who.resists[DamageType.BLIGHT] = ( who.resists[DamageType.BLIGHT] or 0 ) - 25

		game.logPlayer(who, "#00FF00#The crystal is attuned with fire, it makes you weaker toward cold, dark and blight damage!")

		game.party:learnLore("embers-of-creation-godly-body")
		game.player:setQuestStatus("start-burningcoal", engine.Quest.COMPLETED, "absorbed-gerlyk-fragment")
				
		return {used=true, id=true, destroy=true}
	end}
}
