-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2016 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

name = "Looking for easy clues"
desc = function(self, who)
	local knowGerlyk = game.party:knownLore("creation-human")
	local desc = {}
	desc[#desc+1] = "You don't really need to do this, but why not? There are potential clues in lots of places, these are good ones to start with, all of them easy!\n"
	
	desc[#desc+1] = "Trollmire is a forest near the halfling town of Derth, it has lots of troublesome trolls that bother the halflings."
	if self:isCompleted("trollmire") then
		if self:isCompleted("trollmire-flooded") then
			desc[#desc+1] = "#LIGHT_GREEN#* You have explored the Trollmire and vanquished Shax the Troll, but didn't found anything interesting.#WHITE#"
		else
			desc[#desc+1] = "#LIGHT_GREEN#* You have explored the Trollmire and vanquished Prox the Troll, but didn't found anything interesting.#WHITE#"
		end
	else
		desc[#desc+1] = "#SLATE#* You must explore the Trollmire and find out what lurks there!#WHITE#"
	end

	desc[#desc+1] = "\nKor'Pul are some ruins also near Derth, there are rumours there are some evil creature inside."
	if self:isCompleted("kor-pul") then
		if self:isCompleted("kor-pul-invaded") then
			desc[#desc+1] = "#LIGHT_GREEN#* You have explored the ruins of Kor'Pul and vanquished the Possessed, but didn't found anything interesting, Kor'Pul maybe is really dead.#WHITE#"
		else
			desc[#desc+1] = "#LIGHT_GREEN#* You have explored the ruins of Kor'Pul and vanquished the Shade, but didn't found anything interesting, Kor'Pul maybe is really dead.#WHITE#"
		end
	else
		desc[#desc+1] = "#SLATE#* You must explore the ruins of Kor'Pul and find out what lurks there!#WHITE#"
	end

	desc[#desc+1] = "\nYou have heard that within the scintillating caves on the Shaloren lands lie strange crystals imbued with Spellblaze energies."
	if self:isCompleted("spellblaze") then
		desc[#desc+1] = "#LIGHT_GREEN#* You have explored the scintillating caves and destroyed the Spellblaze Crystal. You found lots of living crystals there, seemly a corruption of Spellblaze, also you found evidence that someone intend to use the leftovers of Spellblaze in some kind of quest for power, finding that person is an interesting idea.#WHITE#"
	else
		desc[#desc+1] = "#SLATE#* You must explore the scintillating caves.#WHITE#"
	end

	desc[#desc+1] = "\nThere are also rumours of a renegade Shaloren camp to the west of their capital."
	if self:isCompleted("rhaloren") then
		desc[#desc+1] = "#LIGHT_GREEN#* You have explored the Rhaloren camp and killed the Inquisitor. You found out the Rhaloren are Shaloren that want to use magic in the open again, also their real leader wasn't present.#WHITE#"
		if self:isCompleted("spellblaze") then
			desc[#desc+1] = "#LIGHT_GREEN#* You wonder if the Rhaloren leader and the Scintillating Caves explorer is the same person, regardless, you must find those people later, seemly they (or 'him') went toward the fiery spellblazed forest.#WHITE#"
		end
	else
		desc[#desc+1] = "#SLATE#* You must explore the renegade Shaloren camp.#WHITE#"
	end

	desc[#desc+1] = "\nThe Thaloren forest is disrupted. Corruption is spreading. Norgos the guardian bear is said to have gone mad."	
	if self:isCompleted("norgos") then
		if self:isCompleted("norgos-invaded") then
			desc[#desc+1] = "#LIGHT_GREEN#* You have explored Norgos' Lair and stopped the shivgoroth invasion. Something is amiss there, but you failed to figure out what.#WHITE#"
		else
			desc[#desc+1] = "#LIGHT_GREEN#* You have explored Norgos' Lair and put it to rest. Something is amiss there, but you failed to figure out what.#WHITE#"
		end
	else
		desc[#desc+1] = "#SLATE#* You must explore Norgos' Lair.#WHITE#"
	end

	desc[#desc+1] = "\nOn the western border of the Thaloren forest a gloomy aura has been set up. Things inside are... twisted."
	if self:isCompleted("heart-gloom") then
		if self:isCompleted("heart-gloom-purified") then
			desc[#desc+1] = "#LIGHT_GREEN#* You have explored the Heart of the Gloom and slain the Dreaming One. Something is amiss there, but you failed to figure out what.#WHITE#"
		else
			desc[#desc+1] = "#LIGHT_GREEN#* You have explored the Heart of the Gloom and slain the Withering Thing. Something is amiss there, but you failed to figure out what.#WHITE#"
		end
	else
		desc[#desc+1] = "#SLATE#* You must explore the Heart of the Gloom.#WHITE#"
	end

	if self:isStatus(engine.Quest.DONE) then
		--if knowGerlyk then
		--	desc[#desc+1] = "#LIGHT_GREEN#* You learned that the legends about Gerlyk creating the human race are probably real, and somehow you became like the first human, or maybe even better, having Gerlyk's Burning Coal as your heart, giving you enormous powers.#WHITE#"
		--else
			desc[#desc+1] = "\n#LIGHT_GREEN#* You explored lots of places, killed lots of unique creatures, but you only learned scraps of information, maybe not even that.#WHITE#"
		--end	
	end

	return table.concat(desc, "\n")
end

on_status_change = function(self, who, status, sub)
	--nothign for now
end

allDone = function(self)
	return self:isCompleted("trollmire") and self:isCompleted("kor-pul") and self:isCompleted("spellblaze") and self:isCompleted("rhaloren") and self:isCompleted("norgos") and self:isCompleted("heart-gloom")
end
