-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2016 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

name = "The Burning Coals of Creation"
desc = function(self, who)
	local knowGerlyk = game.party:knownLore("creation-human")
	local desc = {}
	desc[#desc+1] = "Explore the Embers of Creation caves, and find what you are looking for, whatever it is.\n"
	if self:isCompleted("entered") then
		desc[#desc+1] = "#LIGHT_GREEN#* You entered the cave, and now feel your chest burning, you need to find out why.#WHITE#"

		if self:isCompleted("absorbed-gerlyk-aura") and self:isCompleted("absorbed-gerlyk-fragment") and self:isCompleted("absorbed-gerlyk-blood") then
			if knowGerlyk then
				desc[#desc+1] = "#LIGHT_GREEN#* You found Gerlyk's Fragment, absorbed some of his lingering aura, and found a knife that was used to stab him.#WHITE#"
			else
				desc[#desc+1] = "#LIGHT_GREEN#* You found some god fragment, absorbed some of his lingering aura, and found a knife that was used to stab him.#WHITE#"
			end

			if self:isCompleted("killed-alchemist") then
				if knowGerlyk then
					desc[#desc+1] = "#LIGHT_GREEN#* You also killed an alchemist that was experimenting with Gerlyk's remnants and attacked you. You found with him a potion that is essentially an extract of Gerlyk's essense.#WHITE#"
				else
					desc[#desc+1] = "#LIGHT_GREEN#* You also killed an alchemist that was experimenting with some god remnants and attacked you. You found with him a potion that is essentially an extract of a god essense.#WHITE#"
				end
			end

			if self:isCompleted("killed-bone-golem") then
				desc[#desc+1] = "#LIGHT_GREEN#* You killed the gigantic bone golem that was blocking the exit!#WHITE#"
			elseif not self:isEnded() then
				desc[#desc+1] = "#SLATE#* Now all you need to do is find the exit!#WHITE#"
			end
		else
			local anyCompleted = false

			if self:isCompleted("killed-alchemist") then
				anyCompleted = true
				if self:isCompleted("absorbed-gerlyk-fragment") then
					if knowGerlyk then
						desc[#desc+1] = "#LIGHT_GREEN#* You killed an alchemist that was experimenting with Gerlyk's remnants and attacked you. You found with him a potion that is essentially an extract of Gerlyk's essense.#WHITE#"
					else
						desc[#desc+1] = "#LIGHT_GREEN#* You killed an alchemist that was experimenting with some god remnants and attacked you. You found with him a potion that is essentially an extract of a god essense.#WHITE#"
					end
				else
					desc[#desc+1] = "#LIGHT_GREEN#* You killed an alchemist that was also exploring the place and attacked you.#WHITE#"
				end
			end

			if self:isCompleted("absorbed-gerlyk-aura") then
				anyCompleted = true
				if knowGerlyk then
					desc[#desc+1] = "#LIGHT_GREEN#* You encountered the lingering remnants of Gerlyk's power, in the form of an aura.#WHITE#"
				else
					desc[#desc+1] = "#LIGHT_GREEN#* You encountered the lingering remnants of some god power, in the form of an aura.#WHITE#"
				end
			end	

			if self:isCompleted("absorbed-gerlyk-fragment") then
				anyCompleted = true
				if knowGerlyk then
					desc[#desc+1] = "#LIGHT_GREEN#* You found a red crystal, it is pure condensed power of Gerlyk.#WHITE#"
				else
					desc[#desc+1] = "#LIGHT_GREEN#* You found a red crystal, it is pure condensed power of some god that likes fire.#WHITE#"
				end
			end

			if self:isCompleted("absorbed-gerlyk-blood") then
				anyCompleted = true
				if knowGerlyk then
					desc[#desc+1] = "#LIGHT_GREEN#* You a knife soaked with dried blood, that probably belonged to Gerlyk, you wonder if the knife is actually real, since you doubt someone can actually stab him.#WHITE#"
				else
					desc[#desc+1] = "#LIGHT_GREEN#* You a knife soaked with dried blood, you wonder if the knife is actually real, since you doubt someone can actually stab the powerful creature that was the source of the blood.#WHITE#"
				end
			end	

			if not self:isEnded() then	
				if anyCompleted then
					desc[#desc+1] = "#SLATE#* You must keep exploring to find out the source of the things you found so far.#WHITE#"
				else
					desc[#desc+1] = "#SLATE#* You must explore the place to find out why your chest is burning.#WHITE#"
				end
			end
		end
	elseif not self:isEnded() then
		desc[#desc+1] = "#SLATE#* You must find the way out of the forest and into the cave below.#WHITE#"
	end	

	if self:isStatus(engine.Quest.DONE) then
		if knowGerlyk then
			desc[#desc+1] = "#LIGHT_GREEN#* You learned that the legends about Gerlyk creating the human race are probably real, and somehow you became like the first human, or maybe even better, having Gerlyk's Burning Coal as your heart, giving you enormous powers.#WHITE#"
		else
			desc[#desc+1] = "#LIGHT_GREEN#* You left place after absorbing lots of godly power, you still feel you could learn more about this though. But you must seek elsewhere, on the world at large.#WHITE#"
		end
	elseif self:isStatus(engine.Quest.FAILED) then
		desc[#desc+1] = "#RED#* You left the Embers of Creation before you could learn all that you could.#WHITE#"
	end

	return table.concat(desc, "\n")
end

on_status_change = function(self, who, status, sub)
	--nothign for now
end

allDone = function(self)
	return self:isCompleted("entered") and self:isCompleted("absorbed-gerlyk-aura") and self:isCompleted("absorbed-gerlyk-fragment") and self:isCompleted("absorbed-gerlyk-blood") and self:isCompleted("killed-bone-golem")
end
