local x, y = game.state:findEventGrid(level)
if not x then return false end

if not game.zone.dangerChestsOpened then game.zone.dangerChestsOpened = 0 end

local o
local r = rng.range(0, 99)
if r < 10 then
	o = game.state:generateRandart{lev=resolvers.current_level+10}
elseif r < 40 then
	o = game.zone:makeEntity(game.level, "object", {tome={double_greater=1}}, nil, true)
else
	o = game.zone:makeEntity(game.level, "object", {tome={greater_normal=1}}, nil, true)
end
r = game.zone.dangerChestsOpened + 99 - r
local ms
if rng.percent(r * 2) then
	ms = {}
	r = rng.range(0, 99) - game.zone.dangerChestsOpened
	if r < 1 then
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {random_boss=true}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {random_elite=true}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {random_elite=true}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {}, nil, true)
	elseif r < 8 then
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {random_boss=true}, nil, true)
	elseif r < 25 then
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {random_elite=true}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {random_elite=true}, nil, true)
	elseif r < 60 then
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {random_elite=true}, nil, true)
	else
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {}, nil, true)
		ms[#ms+1] = game.zone:makeEntity(game.level, "actor", {}, nil, true)
	end
end

local g = game.level.map(x, y, engine.Map.TERRAIN):cloneFull()
g.name = "treasure chest"
g.display='~' g.color_r=255 g.color_g=215 g.color_b=0 g.notice = true
g.always_remember = true g.special_minimap = {b=150, g=50, r=90}
g:removeAllMOs()
if engine.Map.tiles.nicer_tiles then
	g.add_displays = g.add_displays or {}
	g.add_displays[#g.add_displays+1] = mod.class.Grid.new{image="object/chest1.png", z=5}
end
g:altered()
g.special = true
g.chest_item = o
g.chest_guards = ms
g.block_move = function(self, x, y, who, act, couldpass)
	if not who or not who.player or not act then return false end
	if self.chest_opened then return false end

	require("engine.ui.Dialog"):yesnoPopup("Treasure Chest", "Open the chest?", function(ret) if ret then
		self.chest_opened = true
		game.zone.dangerChestsOpened = game.zone.dangerChestsOpened + 1
		if self.chest_item then
			game.zone:addEntity(game.level, self.chest_item, "object", x, y)
			game.logSeen(who, "#GOLD#An object rolls from the chest!")
			if self.chest_guards then
				for _, m in ipairs(self.chest_guards) do
					if game.level.data and game.level.data.special_level_faction then
						m.faction = game.level.data.special_level_faction
					end
					local mx, my = util.findFreeGrid(x, y, 5, true, {[engine.Map.ACTOR]=true})
					if mx then game.zone:addEntity(game.level, m, "actor", mx, my) end
				end
				game.logSeen(who, "#GOLD#But the chest was guarded!")
			end
		end
		self.chest_item = nil
		self.chest_guards = nil
		self.block_move = nil
		self.special = nil
		self.autoexplore_ignore = true
		self.name = "treasure chest (opened)"

		if self.add_displays and self.add_displays[1] then
			self.add_displays[1].image = "object/chestopen1.png"
			self:removeAllMOs()
			game.level.map:updateMap(x, y)
		end
	end end, "Open", "Leave")

	return false
end
game.zone:addEntity(game.level, g, "terrain", x, y)
print("[EVENT] treasure-chest placed at ", x, y)
return true
