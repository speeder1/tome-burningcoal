return [[You don't remember exactly how you got into this forest, you do remember that you were seeking something, or someone, someone important.

You also notice you have a rune on your body, while you don't like magic, it certainly would be useful, you can feel it is a very powerful one,
somehow you cannot remember how it got there though, it was probably inscribed into you forcefully by someone on Last Hope, or nearby.]]